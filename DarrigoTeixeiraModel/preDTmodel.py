import numpy as np
import matplotlib.pyplot as plt

X, I0_G, sI0_G, Rg_G, sRg_G, I0_OZ, sI0_OZ, Rg_OZ, sRg_OZ = \
		np.loadtxt("../Ajustes/ajustes.dat", usecols=(1, 2, 3, 4, 5, 8, 9, 10, 11), max_rows=7, unpack=True)


wIG = 1./sI0_G**2
wRG = 1./sRg_G**2
wIOZ = 1./sI0_OZ**2
wROZ = 1./sRg_OZ**2

I0 = (I0_G*wIG + I0_OZ*wIOZ)/(wIG + wIOZ)
sI0 = np.sqrt(1./(wIG + wIOZ))
Rg = (Rg_G*wRG + Rg_OZ*wROZ)/(wRG + wROZ)
sRg = np.sqrt(1./(wRG + wROZ))


raio = np.sqrt(5./3.)*Rg
sraio = np.sqrt(5./3.)*sRg

vP = (4.*np.pi/3.)*(raio**3)
svP = (4.*np.pi)*(raio**2)*sraio

arq = open("data_DTmodel.dat", 'w')
arq.write("#X\t I0\t\t sI0\t\t Rg\t\t sRg\t\t vP\t\t svP\n")
for i in range(len(X)):
	arq.write("%.3f\t %.5e\t %.2e\t %.5e\t %.2e\t %.5e\t %.2e\n" % (X[i], I0[i], sI0[i], Rg[i], sRg[i], vP[i], svP[i]))

arq.close()


# Gráfico do volume.
plt.title(u'Volume dos agregados moleculares\n')

plt.plot(X, vP/1e3, ls='', marker='o', ms=5, mec='#00aacc', mfc='#00aacc', label=u'Volume médio\n dos agregados')
plt.errorbar(X, vP/1e3, yerr=svP/1e3, ls='', ecolor='k', capsize=3)

#plt.yticks(np.arange(4, 15, 1))
#plt.xticks(np.linspace(0.1, 0.3, 9))
plt.xlim(0.09,0.31)
plt.xlabel(r'Fração molar de terc-butanol')
plt.ylabel(r'Volume médio dos agregados$ \quad (\mathrm{nm^{3}})$')
plt.legend(loc='best')

plt.savefig("Graficos/volumes.pdf")

plt.show()
