# Esse programa mapeia os parâmetros do modelo do Teixeira (1990) para   \
#        descrever a natureza dos complexos de água:t-butanol através da \
#        intensidade de espalhamento SAXS.
# Considera-se que os dados estão A³ e cm^{-1}, o programa trabalha com unidades de A³ e eu/A³.
# A saída é ainda em eu/A³. # ñ é convertida de volta para cm^{-1}.

import numpy as np
import numba
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib.colors import BoundaryNorm
from matplotlib.colors import SymLogNorm
from numba import jit
from time import time

@jit(nopython=True)#
def DTModelCalc(X, I0e, sI0e, vP, R, F):
	# Constantes.
	bA = 10
	vA = 29.915
	bB = 42
	vB = 158.82

	# Modelo e cálculos.
	m = R*(1-X)*vP/(R*(1-X)*vA + F*X*vB)
	n = (vP - m*vA)/vB
	n_P = (F/n)*X/((1-X)*vA + X*vB)
	
	phi_P = n_P*vP
#    rho_P = (R*(1-X)*bA + F*X*bB)/vP #Essa é a equação do teixeira que está \
			#ERRADA
	rho_P = (m*bA + n*bB)/vP # Essa aqui tá OK.
	rho_0 = ((1-R)*(1-X)*bA + (1-F)*X*bB)/((1-R)*(1-X)*vA + (1-F)*X*vB)
	D_rho2 = (rho_P - rho_0)**2
	I0t = n_P*D_rho2*vP*vP

	D_I0 = (I0t - I0e)/sI0e
	return D_I0, m, n, n_P, rho_P, D_rho2, phi_P

def DTModelSave(nome, R, F, D_I0, m, n, n_P, rho_P, D_rho2, phi_P, Dscale):
	# Salva os dados de desvio ~ 0 em arquivo.
	j, k = np.where(np.abs(D_I0) <= Dscale)
	with open(("outDTmodel/" + nome + ".dat"), 'w') as arq:
		arq.write("# r\tf\tD_I0\tm\tn\tn_P\trho_P\tD_rho2\tphi_P\n")
		for a in range(0, len(j)):
			ja = j[a]
			ka = k[a]
			arq.write("%.6f\t\t%.6f\t\t%.6f\t\t%.2f\t\t%.2f\t\t%.4e\t\t%.3e\t\t%.3e\t\t%.2f\n" % (R[ja][ka], \
					F[ja][ka], D_I0[ja][ka], m[ja][ka], \
					n[ja][ka], n_P[ja][ka], rho_P[ja][ka], D_rho2[ja][ka], \
					(100*phi_P[ja][ka])))
					#
				#
			#
		#

def DTModelGraph(X, nome, R, F, D_I0, Dscale):
	# Gráfico.
	levels = MaxNLocator(nbins=50).tick_values(-Dscale, Dscale)
	cmap = plt.get_cmap('twilight_shifted_r')
	#norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
	norm = SymLogNorm(linthresh=1., linscale=0.5, vmin=-10000, vmax=10000, clip=True)
	fig, ax0 = plt.subplots(1, figsize=[2*5, 2*4])
	im = ax0.pcolormesh(R, F, D_I0, cmap=cmap, norm=norm)
	tbar = np.array([-1000, -100., -10., -1., 0., 1., 10., 100., 1000])
	lbar = [r"$-10^3$", r"$-10^2$", r"$-10$", r"$-1$", r"$0$", r"$+1$", r"$+10$", r"$+10^2$", r"$+10^3$"]
	cb = fig.colorbar(im, ax=ax0, anchor=(0, 0.5), ticks=tbar)
	cb.set_label(r"$\Delta\,I_0 \quad (\sigma_{I_0})$", fontsize=18)
	cb.ax.set_yticklabels(lbar)
	cb.ax.tick_params(labelsize=16)
	
	ax0.set_title(('Desvio de $I_0$ teórica em relação ao ' +\
			'experimento para ' + (r'$X = %.3f$' % X) + '\n'), fontsize=20)
	ax0.set_xlabel(r'$r$ - fração de água nos aglomerados', fontsize=18)
	ax0.set_ylabel(r'$f$ - fração de t-butanol nos aglomerados', fontsize=18)
	ax0.tick_params('both', labelsize=16)
	plt.rc('legend', fontsize=16)
	
	#ax0.ticklabel_format(style='sci', scilimits=(0,0), useMathText=True)
	fig.tight_layout()
	plt.savefig(("Graficos/" + nome + ".jpeg"))
	#plt.show()

# Dados.
X, I0e, sI0e, vP, svP = np.loadtxt("data_DTmodel.dat",  usecols=(0, 1, 2, 5, 6), unpack=True)

# Conversão das unidades de I0.
I0e /= 7.941124e-2 
sI0e /= 7.941124e-2 

# Parâmetros
limR = 1
limF = 1
# 5000 é o MÁXIMO que a memória comporta, mas não pode ter MAIS NADA ABERTO. 3000 vai a 30% já.
dr = df = min([limR, limF])/3000.
# Constrói a grade de parâmetros.
R, F = np.mgrid[slice(0.0000001, limR, dr), slice(0.0000001, limF, df)]

# Escala de desvios.
Dscale = 3.

for i in range(len(X)):
	nome=("%.3f_outDTmodel" % X[i])
	nome = nome.replace('.', '')
	#
	start_time = time()
	D_I0, m, n, n_P, rho_P, D_rho2, phi_P = DTModelCalc(X[i], I0e[i], sI0e[i], vP[i], R, F)
	print("--- %.4f seconds ---" % (time() - start_time))
	#
	DTModelSave(nome, R, F, D_I0, m, n, n_P, rho_P, D_rho2, phi_P, Dscale)
	#
	#DTModelGraph(X[i], nome, R, F, D_I0, Dscale)
