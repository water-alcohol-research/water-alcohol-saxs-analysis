#! /usr/bin/python

# Este programa realiza ajustes de Guinier e de Ornstein-Zernike dos dados \
# de uma curva de espalhamento SAXS.

# Esta é a terceira versão do programa. Corrigida para utilizar a matriz de \
# covariância padrão, construir os gráficos na mesma escala (versão 2).
# A terceira versão foi adaptada para incluir a realização de ajuste por \
# Ornstein-Zernike (OZ).


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tick
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

############### Define uma classe com o formato dos dados do SAXS. #############
################################################################################

class Saxs():
    def __init__(self, Nq):
        self.q = np.zeros(Nq)
        self.I = np.zeros(Nq)
        self.sI = np.zeros(Nq)
        self.lnI = np.zeros(Nq)
    
################################################################################



################### Importa os dados de média já computados. ###################
################################################################################

def ImportData(entrada):
    lines = np.loadtxt(entrada, usecols=1, unpack=True)
    NumberData = len(lines)
    
    dados = Saxs(NumberData)
    dados.q, dados.I, dados.sI = np.loadtxt(entrada, usecols=(0,1,2), unpack=True)
    
    for i in range(NumberData):
        if(dados.I[i]>0):
            dados.lnI[i] = np.log(dados.I[i])
        else:
            dados.lnI[i] = -8 #min(dados.lnI)
    
    return dados
    
def SelecaoIntervalo(saxs, X):
    # Seleção de limite pelo gráfico de Guinier
    ################################################################################
    x = saxs.q**2
    y = saxs.lnI
    s = saxs.sI/saxs.I

    fig, ax = plt.subplots(1)
    ax.plot(x, y, ls='-', marker='', markersize=1, color='blue', label=u'Dados')
    ax.errorbar(x, y, yerr=s, fmt='none', capsize=0, ecolor='gray')
    ax.set_title(("Gráfico de Guinier %.3f" % (X)))
    plt.legend(loc='upper right')
    #ax.set_xlim(-0.002,0.202)
    #ax.set_ylim(-8, -1)
    #plt.xticks(np.linspace(0,0.21,11))
    #plt.yticks(np.linspace(-8,-1,15))
    ax.tick_params(axis='both', labelsize=10)
    ax.set_xlabel(r'$q^2 \, (\AA^{-2})$', size=12)
    ax.set_ylabel(r'$\ln(I)$', size=12)
    plt.grid()

    #plt.draw()
    plt.show(block=False)
    #plt.show(fig, block=False)
    
    # Define limitadores (cutless, cutgreat).
    xamin = float(input("Entre com o limite inferior dos dados para o ajuste: "))
    xamax = float(input("Entre com o limite superior dos dados para o ajuste: "))

    cutmin = float(input("Corte o gráfico para q^2 menor que: "))
    cutmax = float(input("Corte o gráfico para q^2 maior que: "))
    
    plt.close()
    
    return xamin, xamax, cutmin, cutmax

def GuinierFit(arqname, X, saxs, xamin, xamax, cutmin, cutmax):
    # Ajuste de Guinier
    ################################################################################
    x = saxs.q**2
    y = saxs.lnI
    s = saxs.sI/saxs.I

    # Seleciona os dados para o ajuste.
    xa = x[x[:] > xamin]
    ni = len(x) - len(xa)
    xa = xa[xa[:] < xamax]
    nf = ni + len(xa)

    ya = y[ni:nf]
    sa = s[ni:nf]

    # Cortes no gráfico
    x = x[x[:]<cutmax]
    y = y[0:len(x)]
    s = s[0:len(x)]
    x = x[x[:]>cutmin]
    y = y[len(y)-len(x):]
    s = s[len(s)-len(x):]
    # Limitadores de gráfico.
    yamax = max(ya)
    yamin = min(ya)
    dax = xamax - xamin
    day = yamax - yamin
    ymax = max(y)
    ymin = min(y)
    xmax = max(x)
    xmin = min(x)
    dx = xmax - xmin
    dy = ymax - ymin

    # Ajuste linear da aproximação de Guinier.
    pol, cov = np.polyfit(xa, ya, 1, w=1/sa, cov='unscaled')
    pol, residuals, rank, sing, rcond = np.polyfit(xa, ya, 1, w=1/sa, full=True)

    # Imprime resultado do ajuste.
    print("\n##### Resultados do ajuste de Guinier #####\n")
    print("Arquivo de dados: %s" % arqname)
    print("Fração molar: %.3f" % X)
    print("Intervalo do ajuste (q²): [%.4f, %.4f]\n" % (xamin, xamax))
    print("####################################\n")
    
    print("Coef. angular: %.4e +/- %.1e" % (pol[0], np.sqrt(cov[0][0])))
    print("Coef. linear: %.4e +/- %.1e\n" % (pol[1], np.sqrt(cov[1][1])))
    
    N = len(xa)
    yam = np.sum(ya/(sa**2))/np.sum(1/(sa**2))
    #print(yam)
    tss = np.sum(((ya - yam)/sa)**2)
    #print(tss)
    #rss = np.sum(((ya - np.polyval(pol,xa))/sa)**2)
    #print(rss)
    chi2dof = residuals/(N - 2)#sum(((ya - np.polyval(pol,xa)))**2)
    R2 = 1 - residuals/tss
    
    print("chi²: %.2e" % residuals)
    print("chi²_dof: %.2e" % chi2dof)
    print("R²: %.4f\n" % R2)


    Rg = np.sqrt(-3*pol[0])
    s_Rg = 0.5*(3/Rg)*np.sqrt(cov[0][0])
    print("Raio de giração: %.4e +/- %.2e\n" % (Rg, s_Rg))

    I0 = np.exp(pol[1])
    s_I0 = I0*np.sqrt(cov[1][1])
    print("Intensidade a ângulo 0: %.4e +/- %.2e\n" % (I0, s_I0))

    criterio_G = Rg*np.sqrt(xamax)
    print("Rg x qmax = %.2f; deve ser menor que 1.\n" % (criterio_G))

    # Faz os gráficos.
    ya = np.polyval(pol, xa)

    # Escala do detalhe
    fscale = 0.5*dy/day

    # "Cria os plots".
    fig, ax = plt.subplots(1)#, figsize=[8, 6]
    fig.suptitle((r'Gráfico e ajuste de Guinier para $X = %.3f$' % (X)), \
            size=14)
    cor = '#8000ff'

    # Gráfico 1: curva completa.
    ax.plot(x, y, ls='', marker='o', markersize=1, color=cor, label=(u'Dados'))
    ax.plot(xa, ya, ls='-', marker='', color='black', label=(u'Ajuste'))
    ax.errorbar(x, y, yerr=s, fmt='none', capsize=0, ecolor='gray')
    #ax.grid()
    ax.legend(loc='lower left')
    ax.set(xlim=(xmin-0.01*dx,xmax+0.01*dx), ylim=(ymin-0.01*dy,ymax+0.01*dy), xticks=np.linspace(0,0.18,10))
    ax.set_ylabel(r'$\ln(I)$', size=12)
    ax.set_xlabel(r'$q^2 \quad (\mathrm{\AA^{-2}})$', size=12)
    plt.tick_params(axis='both', labelsize=10)

    # Detalhe
    axins = zoomed_inset_axes(ax, fscale, loc=1)
    axins.plot(x,y, ls='', marker='o', markersize=1, color=cor)
    axins.errorbar(x, y, yerr=s, fmt='none', capsize=0, ecolor='gray')
    axins.plot(xa, ya, ls='-', marker='', color='black')
    # sub region of the original image
    axins.set_xlim(xamin, xamax)
    axins.set_ylim(yamin, yamax)
    # set visibility of x-axis as False
    xax = axins.axes.get_xaxis()
    xax = xax.set_visible(False) 
    # set visibility of y-axis as False
    yax = axins.axes.get_yaxis()
    yax = yax.set_visible(False)
    # draw a bbox of the region of the inset axes in the parent axes and
    # connecting lines between the bbox and the inset axes area
    mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

    #fig.draw()
    figname = "./Graficos/" + arqname.split('.')[0] + "_Guinier" + ".pdf"
    fig.savefig(figname)
    plt.show(block=False)
    
    return criterio_G
    
def OrnsteinZernikeFit(arqname, X, saxs, xamin, xamax, cutmin, cutmax):
    ################################################################################
    # Ajuste de Ornstein-Zernike
    ################################################################################
    x = saxs.q**2
    y = 1./saxs.I
    s = saxs.sI/(saxs.I)**2

    # Seleciona os dados para o ajuste.
    xa = x[x[:] > xamin]
    ni = len(x) - len(xa)
    xa = xa[xa[:] < xamax]
    nf = ni + len(xa)

    ya = y[ni:nf]
    sa = s[ni:nf]

    # Cortes no gráfico
    x = x[x[:]<cutmax]
    y = y[0:len(x)]
    s = s[0:len(x)]
    x = x[x[:]>cutmin]
    y = y[len(y)-len(x):]
    s = s[len(s)-len(x):]

    # Limitadores de gráfico.
    yamax = max(ya)
    yamin = min(ya)
    dax = xamax - xamin
    day = yamax - yamin
    ymax = max(y)
    ymin = min(y)
    xmax = max(x)
    xmin = min(x)
    dx = xmax - xmin
    dy = ymax - ymin

    # Ajuste linear da aproximação de OZ.
    pol, cov = np.polyfit(xa, ya, 1, w=1/sa, cov='unscaled')
    pol, residuals, rank, sing, rcond = np.polyfit(xa, ya, 1, w=1/sa, full=True)

    # Imprime resultado do ajuste.
    print("\n##### Resultados do ajuste de Ornstein-Zernike #####\n")
    print("Arquivo de dados: %s" % arqname)
    print("Fração molar: %.3f" % X)
    print("Intervalo do ajuste (q²): [%.4f, %.4f]\n" % (xamin, xamax))
    print("####################################\n")
    
    print("Coef. angular: %.4e +/- %.1e" % (pol[0], np.sqrt(cov[0][0])))
    print("Coef. linear: %.4e +/- %.1e\n" % (pol[1], np.sqrt(cov[1][1])))
    
    N = len(xa)
    yam = np.sum(ya/(sa**2))/np.sum(1/(sa**2))
    #print(yam)
    tss = np.sum(((ya - yam)/sa)**2)
    #print(tss)
    #rss = np.sum(((ya - np.polyval(pol,xa))/sa)**2)
    #print(rss)
    chi2dof = residuals/(N - 2)#sum(((ya - np.polyval(pol,xa)))**2)
    R2 = 1 - residuals/tss
    
    print("chi²: %.2e" % residuals)
    print("chi²_dof: %.2e" % chi2dof)
    print("R²: %.4f\n" % R2)
    
    PersistenceLength = np.sqrt(pol[0]/pol[1])
    s_PL = np.sqrt(((cov[0][0]/(pol[0]*pol[1])) + ((cov[1][1]*pol[0])/((pol[1])**3)))/4)
    Rg = np.sqrt(3)*PersistenceLength
    s_Rg = np.sqrt(3)*s_PL
    print("Raio de giração: %.4e +/- %.2e\n" % (Rg, s_Rg))

    I0 = 1/(pol[1])
    s_I0 = np.sqrt(cov[1][1])/(pol[1]**2)
    print("Intensidade a ângulo 0: %.4e +/- %.2e\n" % (I0, s_I0))

    criterio_OZ = Rg*np.sqrt(xamax)
    print("Rg x qmax = %.2f; deve ser menor que 1.\n\n" % criterio_OZ)

    # Faz os gráficos.
    ya = np.polyval(pol, xa)

    # Escala do detalhe
    fscale = 0.5*dy/day

    # "Cria os plots".
    fig, ax = plt.subplots(1)#, figsize=[8, 6]
    fig.suptitle((r'Gráfico e ajuste de Ornstein-Zernike para $X = %.3f$' % (X)), \
            size=14)
    cor = '#8000ff'

    # Gráfico 1: curva completa.
    ax.plot(x, y, ls='', marker='o', markersize=1, color=cor, label=(u'Dados'))
    ax.plot(xa, ya, ls='-', marker='', color='black', label=(u'Ajuste'))
    ax.errorbar(x, y, yerr=s, fmt='none', capsize=0, ecolor='gray')
    #ax.grid()
    ax.legend(loc='lower right')
    ax.set(xlim=(xmin-0.01*dx,xmax+0.01*dx), ylim=(ymin-0.01*dy,ymax+0.01*dy), xticks=np.linspace(0,0.18,10))
    ax.set_ylabel(r'$I^{-1} \quad (\mathrm{cm})$', size=12)
    ax.set_xlabel(r'$q^2 \quad (\mathrm{\AA^{-2}})$', size=12)
    plt.tick_params(axis='both', labelsize=10)

    # Detalhe
    axins = zoomed_inset_axes(ax, fscale, loc=2)
    axins.plot(x,y, ls='', marker='o', markersize=1, color=cor)
    axins.errorbar(x, y, yerr=s, fmt='none', capsize=0, ecolor='gray')
    axins.plot(xa, ya, ls='-', marker='', color='black')
    # sub region of the original image
    axins.set_xlim(xamin, xamax)
    axins.set_ylim(yamin, yamax)
    # set visibility of x-axis as False
    xax = axins.axes.get_xaxis()
    xax = xax.set_visible(False) 
    # set visibility of y-axis as False
    yax = axins.axes.get_yaxis()
    yax = yax.set_visible(False)
    # draw a bbox of the region of the inset axes in the parent axes and
    # connecting lines between the bbox and the inset axes area
    mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")

    #fig.draw()
    figname = "./Graficos/" + arqname.split('.')[0] + "_OZ" + ".pdf"
    fig.savefig(figname)
    plt.show(block=False)
    
    return criterio_OZ
################################################################################
############################## Inicialização ###################################
################################################################################

#path = input("Entre com a pasta onde estão os dados: ")
path = "Data"
arqname=input("Entre com o nome do arquivo de dados: ")
X = float(input("Entre com a concentração: "))
path_arqname = path + "/" + arqname
saxs = ImportData(path_arqname)

while(True):
    # Define limitadores (cutless, cutgreat).
	xamin, xamax, cutmin, cutmax = SelecaoIntervalo(saxs, X)

	criterio1 = GuinierFit(arqname, X, saxs, xamin, xamax, cutmin, cutmax)
	criterio2 = OrnsteinZernikeFit(arqname, X, saxs, xamin, xamax, cutmin, cutmax)
    
	if ((criterio1>1) or (criterio2>1)):
		teste = bool(input("Continuar tentando? sim = 1, não = 0: "))
		if(not(teste)):
			print("OK")
			break
	else:
		print("Você conseguiu!")
		break

plt.show()
