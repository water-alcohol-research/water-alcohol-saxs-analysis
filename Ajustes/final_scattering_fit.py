import numpy as np
import matplotlib.pyplot as plt

color = ['#0000ff', '#0070a0', '#00b0b0', '#70ff70', '#00b000', '#b000b0', '#e0e000', '#f04000', '#ff0000']

X = np.array([0.10, 0.132, 0.14, 0.145, 0.17, 0.199, 0.20, 0.285, 0.30])

meusArquivos = ['ci4_010.dat', 'am4_0132.dat', 'ci5_014.dat', 'am3_01450.dat', 'ci1_017.dat', 'am2_0199.dat', 'ci3_020.dat', 'am1_0285.dat', 'ci2_0300.dat']
myData = []
for i in range(9):
    meusArquivos[i] = "./Data/" + meusArquivos[i]
    meusDados = np.loadtxt(meusArquivos[i], usecols=(0,1,2), skiprows=0, unpack=True)
    #meusDados[1] /= 7.941124e-3 # Conversão para unidades de espalhamento do elétron.
    myData.append(meusDados)

# Trunca os dados.
qmin = 0.018
qmax = 0.45
for i in range(9):
	ni = len(myData[i][0]) - len(myData[i][0][myData[i][0][:]>qmin])
	nf = len(myData[i][0][myData[i][0][:]<qmax])
	
	myData[i] = (myData[i].T[ni:nf]).T

# Seleciona os limites para o fit do "I_final".
qfit_i = 0.42
qfit_f = 0.44
res = []

# Faz o gráfico de espalhamento.
fig, ax = plt.subplots(1,2,figsize=[12,4.5])#

arq = open("final_scattering_fit.dat", 'w')

arq.write("#I_final\t sI_final\t chi2df\n")

for i in range(0, 9):
	# Faz o fit.
    ni = len(myData[i][0]) - len(myData[i][0][myData[i][0][:]>qfit_i])
    nf = len(myData[i][0][myData[i][0][:]<qfit_f])

    data_fit = (myData[i].T[ni:nf]).T
    
    pol, cov = np.polyfit(data_fit[0], data_fit[1], 0, w=1/data_fit[2], cov='unscaled')
    pol, residuals, rank, sing, rcond = np.polyfit(data_fit[0], data_fit[1], 0, w=1/data_fit[2], full=True)
    
    chi2df = residuals[0]/(len(data_fit[0]) - 1)
    res.append(np.array([pol[0], cov[0,0], chi2df]))
    arq.write("%.4e\t %.2e\t %.2e\n" % (res[i][0], res[i][1], res[i][2]))
    
    # Faz os gráficos.
    #ax0.semilogy(myData[i][0], myData[i][1], marker='x', ls='', ms=1, c=color[i], label=(r"$X = %.3f$" % X[i]))
    ax[0].plot(myData[i][0], myData[i][1], marker='o', ls='-', lw=1, ms=1, c=color[i], label=(r"$%.3f$" % X[i]))
    #ax0.errorbar(myData[i][0], myData[i][1], yerr=myData[i][2], elinewidth=0.1, c='k', capsize=0)#"gray"
    ax[0].plot(data_fit[0], np.polyval(pol, data_fit[0]), marker='', ls='-', lw=2, c='k')

arq.close()
ax[0].legend(loc='best')
ax[0].set_title(u"Intensidade de espalhamento (NanoStar)")
ax[0].set_xlabel(r"$q \quad (\mathrm{\AA^{-1}})$", fontsize=14)
ax[0].set_ylabel(r"$I(q) \quad (\mathrm{cm^{-1}})$", fontsize=14)
#ax0.set_yticks(np.linspace(0, 0.11, 23))
#ax0.grid()

res = np.array(res).T
# Gráfico do X vs I_final
#fig1, ax1 = plt.subplots(1)
for i in range(0, 9):
    ax[1].plot(X[i], res[0][i], ls='', marker='o', ms=5, c=color[i], label=r"$I_{final}$")
    ax[1].errorbar(X[i], res[0][i], yerr=res[1][i], ls='', marker='', ms=5, c='k', capsize=3)
#ax1.plot(X, res[0], ls='', marker='x', ms=3, label=r"$I_{final}$")
#ax1.legend(loc='best')
ax[1].set_title(r"$I_{final}$ vs fração molar")
ax[1].set_xlabel(r"$X$", fontsize=14)
ax[1].set_ylabel(r"$I_{final} \quad (\mathrm{cm^{-1}})$", fontsize=14)

plt.tight_layout()
fig.savefig("Graficos_extra/final_scattering_fit.pdf")
plt.show()

